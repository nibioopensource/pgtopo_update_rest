# Topology update SQL methods for REST API

Generic access methods to functionality of `topo_update`
postgreSQL extension, see

	https://gitlab.com/nibioopensource/pgtopo_update_sql

The APIs are intended to be used via PostgREST, see

	https://postgrest.org/en/stable/

For authentication JSON Web token can be used, see

	https://jwt.io.

# Requirements

	Runtime requires:
		- pgtopo_update_sql EXTENSION
		  version v1.0.0-672-g3886eed or higher

	Testcases expect postgrest version 12.2.3

# Install in database


Just Load the `topo_update_rest.sql` script in the target database

For convenience a `topo_update_rest.sh` script is provided, so you
can use:

	topo_update_rest.sh $DBNAME

