#!/bin/bash

cd $(dirname $0)

INFO_FILE=test_server.info
LOG_FILE=test_server.log

usage() {
  echo "Usage: $(basename $0)"
}

while test -n "$1"; do
  if test "$1" = '--help' || test "$1" = '-h'; then
    usage
    exit 0
  else
    echo "Unrecognized option $1" >&2
    usage >&2
    exit 1
  fi
done

# Check if the server is already started
if test -f test_server.info; then
	# Format of test_server.info is: $PID:$PORT
	SERVER_INFO=$(cat ${INFO_FILE})
	SERVER_PID=$(echo "${SERVER_INFO}" | cut -d: -f1)
	SERVER_PORT=$(echo "${SERVER_INFO}" | cut -d: -f2)
	# TODO: check existance of process and availability
	#       of port
	if ps ${SERVER_PID} > /dev/null; then
		echo "Server info file found (already running): PID=$SERVER_PID PORT=$SERVER_PORT"
		exit 0
	fi
fi

export PGDATABASE=nibio_reg_rest
USER=$(psql -XtAc "select user")

export PGRST_DB_URI="postgres://${USER}@/${PGDATABASE}"
export PGRST_DB_SCHEMA="topo_update_rest"
export PGRST_DB_ANON_ROLE="web_anon" # is this not the default ?
export PGRST_SERVER_PORT=3000 # TODO: automatically find next available port

env | grep PGRST > ${LOG_FILE}

postgrest >> ${LOG_FILE} 2>&1 &
SERVER_PID=$!
sleep 1 # give background job a chance to run

# read postgrest sever port from log file
PGRST_SERVER_PORT=$(
  cat ${LOG_FILE} |
    sed -ne 's/^.*port //p'
)

# Final check, did it fail ?
if ps ${SERVER_PID} > /dev/null; then
	echo "Postgrest started in background with pid ${SERVER_PID}, listening on port ${PGRST_SERVER_PORT}, logs are in ${LOG_FILE}"
	echo "${SERVER_PID}:${PGRST_SERVER_PORT}" > ${INFO_FILE}
else
	echo "Server failed, last lines from the log follow:"
	tail ${LOG_FILE} >&2
	exit 1
fi

