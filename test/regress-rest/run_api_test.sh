#!/bin/bash

usage() {
  echo "Usage: $(basename $0) [OPTIONS] <test> [<test> ...]"
  echo "Options:"
  echo "   -h    Print this message and exit"
  echo "   -v    Be verbose"
  echo "   -e    Save obtained output as expected file"
  echo "   -k    sKip setup of test db and server"
  echo "   -K    Keep setup of test db and server"
}

SERVICEDIR=$(dirname $0)
VERBOSE=no
SKIP_INIT=no
KEEP_SERVICES=no
EXPECTED=no
TESTDB_LINE=
APIURL=http://localhost:3000 # TODO: make server port configurable
DIFF="diff --strip-trailing-cr -u"

# Should we use getopt for support of long options ? Supported on mac ?
while getopts "vhkKe" o; do
    case "${o}" in
        h)
			usage
			exit 0
			;;
        e)
            EXPECTED=yes
            ;;
        v)
            VERBOSE=yes
            ;;
        k)
            SKIP_INIT=yes
            ;;
        K)
            KEEP_SERVICES=yes
            ;;
        *)
            usage >&2
			exit 1
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "$1" ]; then
    usage >&2
	exit 1
fi

TMPDIR=$(mktemp -d /tmp/pgtopo_update_rest-XXX)
FAIL_COUNT=0
PASS_COUNT=0


setup() {
	if test "$SKIP_INIT" != "yes"; then

		cd ${SERVICEDIR}

		echo "Setting up API testing framework"

		# Stop the test server if running
		./stop_test_server.sh -f || exit 1

		# Ensure test db is created and stays alive
		../regress-sql/run_topo_update_rest_test.sh \
			-v \
			--nodrop \
			dbsetup/dbsetup.sql > dbsetup.log 2>&1 ||
		{
			fail "database setup failed" dbsetup.log
			exit 1
		}

		TESTDB_LINE=$(grep 'Creating database ' dbsetup.log)
		echo ${TESTDB_LINE}

		# Start the test server
		# TODO: set a listening port here ?
		./start_test_server.sh || {
			fail "postgrest setup start failed" test_server.log
			exit 1
		}

		cd -
	fi
}

after_test_hook() {
	cd ${SERVICEDIR}

	# Reset test db across tests
	../regress-sql/run_topo_update_rest_test.sh \
		-v \
		--nocreate \
		--nodrop \
		dbsetup/dbreset.sql > dbreset.log 2>&1 ||
	{
		fail "database reset failed" dbreset.log
		exit 1
	}
}

cleanup() {
	if test "$KEEP_SERVICES" = "no"; then

		cd ${SERVICEDIR}

		echo "Bringing down API testing framework" | tee -a dbsetup.log

		# Stop the test server
		./stop_test_server.sh

		# Drop the test db
		TESTDB_LINE=$(grep 'Creating database ' dbsetup.log)
		if test -n "${TESTDB_LINLE}"; then
			TESTDB=$(echo "${TESTDB_LINE}" | sed "s/.*'\([^']*\)'.*/\1/")
			if test -n "${TESTDB}"; then
				echo "Dropping database ${TESTDB}"
				dropdb ${TESTDB}
			fi
		fi

		cd -
	fi
}

trap 'cleanup' 0

fail() {
	MSG="${1}"
	LOG="${2}"
	FAIL_COUNT=$((FAIL_COUNT+1))

	if test -z "${LOG}"; then
		echo "failed (${MSG})"
	else
		echo "failed (${MSG}: ${LOG})"
		if test "${VERBOSE}" = "yes"; then
			echo "-----------------------------------------------------------------------------";
			cat ${LOG}
			echo "-----------------------------------------------------------------------------";
		fi
	fi
}

pass() {
	MSG="$1"
	PASS_COUNT=$((PASS_COUNT+1))
	echo "ok ${MSG}" # TODO: print timing ?
}

runtest() {
	TEST="$1"
	TESTNAME=$(echo "${TEST}" | sed 's/\.[^\.]*//')
	TESTCODE=$(echo "${TESTNAME}" | sed 's@[/ ]@-@g')
	TESTFILE="${TESTNAME}.json"
	EXPFILE="${TESTNAME}_expected"
	LOGFILE=${TMPDIR}/${TESTCODE}.log
	OUTFILE=${TMPDIR}/${TESTCODE}.out
	DIFFILE=${TMPDIR}/${TESTCODE}.dif

	exec 3> ${LOGFILE} # -- fd 3 for log file

	echo -n " ${TESTNAME} .. "

	if ! test -f "${TESTFILE}"; then
		echo "TESTFILE: ${TESTFILE}"
		fail "Test file ${TESTFILE} does not exist"
		return 1
	fi

	if test "${EXPECTED}" = "no" && ! test -f "${EXPFILE}"; then
		fail "expected file ${EXPFILE} does not exist"
		return 1
	fi

	RELURL=$(jq -Mr .endpoint "${TESTFILE}" | sed 's@^/@@')
	FULLURL=${APIURL}/${RELURL}
	METHOD=$(jq -Mr .method "${TESTFILE}")
	if test -z "${METHOD}"; then
		METHOD=GET
	else
		: # TODO: sanitize user input
	fi
	DATA=$(jq -Mc .payload "${TESTFILE}")

	CMDLINE="curl -s"
	if test -n "${DATA}"; then
		echo "$DATA" > ${OUTFILE}.payload
		CMDLINE="${CMDLINE} -H 'Content-Type: application/json'"
		CMDLINE="${CMDLINE} -d @'${OUTFILE}.payload'"
	fi
	CMDLINE="${CMDLINE} -X '$METHOD'"
	CMDLINE="${CMDLINE} '${FULLURL}'"
	echo "Running: ${CMDLINE}" >&3

	# Run curl saving output to ${OUTFILE}
	if ! eval ${CMDLINE}|jq -MS .  > ${OUTFILE} 2>&3; then
		fail "curl failed" "${LOGFILE}"
		return 1
	fi

	# Format API response with jq
	if ! jq -M . < ${OUTFILE} > ${OUTFILE}.jq 2>&3; then
		fail "jq failed" "${LOGFILE}"
		return 1
	fi

	# Save obtained output to expected file, if requested
	if test "${EXPECTED}" = "yes"; then
		if ! cp ${OUTFILE}.jq ${EXPFILE} 2>&3; then
			fail "copy failed" "${LOGFILE}"
			return 1
		fi
		pass "(expected)"
		return 0
	fi

	# Compute diff between expected and obtained
	if ! ${DIFF} "${EXPFILE}" "${OUTFILE}.jq" > ${DIFFILE}; then
		fail "diff expected obtained" "${DIFFILE}"
		return 1
	else
		pass
		return 0
	fi
}


setup

echo
echo "Running tests"
echo

while test -n "${1}"; do
	TEST="${1}"
	shift
	runtest "${TEST}"

	after_test_hook
done

echo
echo "Run tests: $((FAIL_COUNT+PASS_COUNT))"
echo "Failed: ${FAIL_COUNT}"
echo

exit ${FAIL_COUNT}
