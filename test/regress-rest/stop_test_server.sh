#!/bin/bash

cd $(dirname $0)

INFO_FILE=test_server.info
LOG_FILE=test_server.log
FORCE=no

usage() {
  echo "Usage: $(basename $0) [-f]"
}


while test -n "$1"; do
  if test "$1" = '--help' || test "$1" = '-h'; then
    usage
    exit 0
  elif test "$1" = '-f'; then
    FORCE=yes
    shift
  else
    echo "Unrecognized option $1" >&2
    usage >&2
    exit 1
  fi
done

SERVER_INFO=$(cat ${INFO_FILE} 2> /dev/null)
if test -z "${SERVER_INFO}"; then
	if test "${FORCE}" = "no"; then
		echo "No ${INFO_FILE} found (not running?)" >&2
		exit 1
	else
		exit 0
	fi
fi

# Format of test_server.info is: $PID:$PORT
SERVER_PID=$(echo "${SERVER_INFO}" | cut -d: -f1)
SERVER_PORT=$(echo "${SERVER_INFO}" | cut -d: -f2)

if ps ${SERVER_PID} > /dev/null; then
	echo "Killing process with pid ${SERVER_PID} (supposedly a test server listening on port ${SERVER_PORT})"
	# TODO: check that process listening on given port is the expected one
	kill ${SERVER_PID}
else
	echo "Server info file was stale (corresponding process not found)"
fi

rm ${INFO_FILE}
