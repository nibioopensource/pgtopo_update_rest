-- Schema creation is done by
-- ../regress-sql/run_topo_update_rest_test.sh

-- We add roles permission here, see
-- test/fixtures/database/pgtopo_update_rest_test-create-app.sql

-- TODO: provide functions to deal with permissions ?

-- Permission to list applications
GRANT SELECT ON
	topo_update_rest_test_sysdata_webclient_functions.app_config
TO web_anon;

-- Permission to select application config
GRANT USAGE ON SCHEMA
	topo_update_rest_test_sysdata_webclient_functions
TO web_anon;

-- Permission to use topo_update functions
GRANT USAGE ON SCHEMA
	topo_update
TO web_anon;

-- Permission to read contents of app schema
GRANT USAGE ON SCHEMA
	topo_update_rest_test
TO web_anon;

-- Permission to write contents in app schema
GRANT ALL ON ALL TABLES IN SCHEMA
	topo_update_rest_test
TO web_anon;
GRANT ALL ON ALL SEQUENCES IN SCHEMA
	topo_update_rest_test
TO web_anon;

-- Permission to access topology primitives for app `topo_update_rest_test`
GRANT USAGE ON SCHEMA topo_update_rest_test_sysdata_webclient TO web_anon;

-- Permission to write topology primitives for app `topo_update_rest_test`
-- TODO: turn this into a core topology function ?
GRANT ALL ON ALL TABLES
	IN SCHEMA topo_update_rest_test_sysdata_webclient
TO web_anon;
GRANT ALL ON ALL SEQUENCES
	IN SCHEMA topo_update_rest_test_sysdata_webclient
TO web_anon;
