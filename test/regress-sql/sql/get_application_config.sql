SET client_min_messages TO WARNING;

SELECT 'te1', jsonb_pretty(
	topo_update_rest.get_application_config('{ "a": "b" }'::jsonb)
);

SELECT 't1', jsonb_pretty(
	topo_update_rest.get_application_config('{ "app_name": "nonexistent" }'::jsonb)
);

SELECT 't2', jsonb_pretty(
	topo_update_rest.get_application_config('{ "app_name": "topo_update_rest_test" }'::jsonb)
);

