SET client_min_messages TO ERROR;

SELECT 'te1', jsonb_pretty(
	topo_update_rest.add_point($$
		{ "app_name_typo": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te2', jsonb_pretty(
	topo_update_rest.add_point($$
		{ "app_name": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te3', jsonb_pretty(
	topo_update_rest.add_point($$
		{
			"app_name": "topo_update_rest_test",
			"point": {
			}
		}
	$$::jsonb)
);

SELECT 'te4', jsonb_pretty(
	topo_update_rest.add_point($$
		{
			"app_name": "topo_update_rest_test",
			"point": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [ [ 0, 0 ], [ 10, 0 ] ],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

SELECT 't1', jsonb_pretty(
	topo_update_rest.add_point($$
		{
			"app_name": "topo_update_rest_test",
			"point": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [ 0, 0 ],
					"type" : "Point"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

------------------
-- Cleanup
------------------

\i :regdir/../fixtures/database/pgtopo_update_rest_test-reset-schema.sql
