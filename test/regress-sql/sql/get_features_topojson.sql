SET client_min_messages TO WARNING;

-- Missing app_name
SELECT 'te1', jsonb_pretty(
	topo_update_rest.get_features_topojson($$
		{ "app_name_typo": "topo_update_rest_test" }
	$$::jsonb)
);

-- Missing bbox_diagonal
SELECT 'te2', jsonb_pretty(
	topo_update_rest.get_features_topojson($$
		{ "app_name": "topo_update_rest_test" }
	$$::jsonb)
);

-- Disabled app-config operation
BEGIN;
UPDATE
       topo_update_rest_test_sysdata_webclient_functions.app_config
SET cfg = jsonb_concat(
		cfg - 'operations',
		jsonb_build_object('operations', jsonb_build_object())
);
SELECT 'te3', jsonb_pretty(
	topo_update_rest.get_features_topojson($$
		{
			"app_name": "topo_update_rest_test",
			"bbox_diagonal": {
				"type": "LineString",
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4326"
					}
				},
				"coordinates": [[0,0],[10,10]]
			}
		}
	$$::jsonb)
);
ROLLBACK;

-- Valid call, selecting nothing
SELECT 't1', jsonb_pretty(
	topo_update_rest.get_features_topojson($$
		{
			"app_name": "topo_update_rest_test",
			"bbox_diagonal": {
				"type": "LineString",
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4326"
					}
				},
				"coordinates": [[0,0],[10,10]]
			}
		}
	$$::jsonb)
);

SELECT 't2-xx', 'border', id, ST_AsText(geom) FROM topo_update_rest_test.border;
SELECT 't2-xx', 'surface', id, ST_AsText(geom) FROM topo_update_rest_test.surface;

SELECT NULL FROM
	topo_update_rest.add_borders_split_surfaces($$
		{
			"app_name": "topo_update_rest_test",
			"borderset": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [
						[ 0, 0 ], [ 10, 0 ], [ 10, 10 ],
						[ 0, 10 ], [ 0, 0 ]
					],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb);

SELECT 't2', 'border', id, ST_AsText(geom) FROM topo_update_rest_test.border;
SELECT 't2', 'surface', id, ST_AsText(geom) FROM topo_update_rest_test.surface;

-- Valid call, selecting something
SELECT 't2', jsonb_pretty(
	topo_update_rest.get_features_topojson($$
		{
			"app_name": "topo_update_rest_test",
			"bbox_diagonal": {
				"type": "LineString",
				"crs": {
					"type": "name",
					"properties": {
						"name": "EPSG:4326"
					}
				},
				"coordinates": [[0,0],[10,10]]
			}
		}
	$$::jsonb)
);

------------------
-- Cleanup
------------------

\i :regdir/../fixtures/database/pgtopo_update_rest_test-reset-schema.sql
