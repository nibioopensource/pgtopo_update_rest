SET client_min_messages TO WARNING;

-- Missing app_name
SELECT 'te1', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{ "app_name_typo": "topo_update_rest_test" }
	$$::jsonb)
);

-- Missing table_name
SELECT 'te2', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{ "app_name": "topo_update_rest_test" }
	$$::jsonb)
);

-- Missing id
SELECT 'te3', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{
			"app_name": "topo_update_rest_test",
			"table_name": "surface"
		}
	$$::jsonb)
);

-- Missing properties (should this be an error?)
SELECT 'te4', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{
			"app_name": "topo_update_rest_test",
			"table_name": "surface",
			"id": 1
		}
	$$::jsonb)
);

-- Disabled app-config operation
BEGIN;
UPDATE
       topo_update_rest_test_sysdata_webclient_functions.app_config
SET cfg = jsonb_concat(
		cfg - 'operations',
		jsonb_build_object('operations', jsonb_build_object())
);
SELECT 'te5', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{
			"app_name": "topo_update_rest_test",
			"table_name": "surface",
			"id": 1,
			"properties": {}
		}
	$$::jsonb)
);
ROLLBACK;

-- Non-existent table in app-config
SELECT 'te6', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{
			"app_name": "topo_update_rest_test",
			"table_name": "non-existent",
			"id": 1,
			"properties": {}
		}
	$$::jsonb)
);

-- t1: No update (record not found)
SELECT 't1', 'surface-count', count(*)
FROM topo_update_rest_test.surface;
SELECT 't1', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{
			"app_name": "topo_update_rest_test",
			"table_name": "surface",
			"id": 1,
			"properties": { "label": "t1" }
		}
	$$::jsonb)
);

-- Insert one record
INSERT INTO topo_update_rest_test.surface (label) values ('initial');

-- t2: No update (no properties)
SELECT 't2', 'surface-before', *
FROM topo_update_rest_test.surface ORDER BY id;
SELECT 't2', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{
			"app_name": "topo_update_rest_test",
			"table_name": "surface",
			"id": 1,
			"properties": {}
		}
	$$::jsonb)
);
SELECT 't2', 'surface-after', *
FROM topo_update_rest_test.surface ORDER BY id;

-- t3: real update
SELECT 't3', 'surface-before', *
FROM topo_update_rest_test.surface ORDER BY id;
SELECT 't3', jsonb_pretty(
	topo_update_rest.update_attributes($$
		{
			"app_name": "topo_update_rest_test",
			"table_name": "surface",
			"id": 1,
			"properties": { "label": "t3" }
		}
	$$::jsonb)
);
SELECT 't3', 'surface-after', *
FROM topo_update_rest_test.surface ORDER BY id;

------------------
-- Cleanup
------------------

\i :regdir/../fixtures/database/pgtopo_update_rest_test-reset-schema.sql
