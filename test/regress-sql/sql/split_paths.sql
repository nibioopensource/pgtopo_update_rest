SET client_min_messages TO WARNING;

SELECT 'te1', jsonb_pretty(
	topo_update_rest.split_paths($$
		{ "app_name_typo": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te2', jsonb_pretty(
	topo_update_rest.split_paths($$
		{ "app_name": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te3', jsonb_pretty(
	topo_update_rest.split_paths($$
		{
			"app_name": "topo_update_rest_test",
			"pathset": {
			}
		}
	$$::jsonb)
);

SELECT 't1', jsonb_pretty(
	topo_update_rest.split_paths($$
		{
			"app_name": "topo_update_rest_test",
			"pathset": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [ [ 0, 0 ], [ 10, 0 ] ],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

SELECT 't2-prep-1', jsonb_pretty(
	topo_update_rest.add_path($$
		{
			"app_name": "topo_update_rest_test",
			"path": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [
						[ 0, 10 ], [ 0, 0 ]
					],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

SELECT 't2', jsonb_pretty(
	topo_update_rest.split_paths($$
		{
			"app_name": "topo_update_rest_test",
			"pathset": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [
						[ -5, 5 ], [ 5, 5 ]
					],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

------------------
-- Cleanup
------------------

\i :regdir/../fixtures/database/pgtopo_update_rest_test-reset-schema.sql
