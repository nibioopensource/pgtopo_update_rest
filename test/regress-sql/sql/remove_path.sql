SET client_min_messages TO ERROR;

SELECT 'te1', jsonb_pretty(
	topo_update_rest.remove_path($$
		{ "app_name_typo": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te2', jsonb_pretty(
	topo_update_rest.remove_path($$
		{ "app_name": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te3', jsonb_pretty(
	topo_update_rest.remove_path($$
		{
			"app_name": "topo_update_rest_test",
			"path_id": 1
		}
	$$::jsonb)
);

SELECT 't1-prep-1', jsonb_pretty(
	topo_update_rest.add_path($$
		{
			"app_name": "topo_update_rest_test",
			"path": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [
						[ 0, 10 ], [ 0, 0 ]
					],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

SELECT 't1', jsonb_pretty(
	topo_update_rest.remove_path(
		jsonb_set(
			'{"app_name": "topo_update_rest_test"}',
			'{path_id}',
			to_jsonb( (
				SELECT id
				FROM topo_update_rest_test.path
				WHERE ST_Contains(geom,  'SRID=4326;POINT(0 5)')
			) )
		)
	)
);

------------------
-- Cleanup
------------------

\i :regdir/../fixtures/database/pgtopo_update_rest_test-reset-schema.sql
