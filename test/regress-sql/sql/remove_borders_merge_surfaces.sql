SET client_min_messages TO ERROR;

SELECT 'te1', jsonb_pretty(
	topo_update_rest.remove_borders_merge_surfaces($$
		{ "app_name_typo": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te2', jsonb_pretty(
	topo_update_rest.remove_borders_merge_surfaces($$
		{ "app_name": "topo_update_rest_test" }
	$$::jsonb)
);

SELECT 'te3', jsonb_pretty(
	topo_update_rest.remove_borders_merge_surfaces($$
		{
			"app_name": "topo_update_rest_test",
			"border_id": 1
		}
	$$::jsonb)
);

SELECT 't1-prep-1', jsonb_pretty(
	topo_update_rest.add_borders_split_surfaces($$
		{
			"app_name": "topo_update_rest_test",
			"borderset": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [
						[ 0, 0 ], [ 10, 0 ], [ 10, 10 ],
						[ 0, 10 ], [ 0, 0 ]
					],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

SELECT 't1-prep-2', jsonb_pretty(
	topo_update_rest.add_borders_split_surfaces($$
		{
			"app_name": "topo_update_rest_test",
			"borderset": {
				"geometry" : {
					"crs" : {
						"properties" : {
							"name" : "EPSG:4326"
						},
						"type" : "name"
					},
					"coordinates" : [
						[ 0, 5 ], [ 10, 5 ]
					],
					"type" : "LineString"
				},
				"properties" : {},
				"type" : "Feature"
			}
		}
	$$::jsonb)
);

SELECT 't1', jsonb_pretty(
	topo_update_rest.remove_borders_merge_surfaces(
		jsonb_set(
			'{"app_name": "topo_update_rest_test"}',
			'{border_id}',
			to_jsonb( (
				SELECT id
				FROM topo_update_rest_test.border
				WHERE ST_Contains(geom,  'SRID=4326;POINT(5 5)')
			) )
		)
	)
);

------------------
-- Cleanup
------------------

\i :regdir/../fixtures/database/pgtopo_update_rest_test-reset-schema.sql
