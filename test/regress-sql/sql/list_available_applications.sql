SET client_min_messages TO WARNING;

SELECT 't1', jsonb_pretty(
	topo_update_rest.list_available_applications()
);

BEGIN;
	REVOKE SELECT ON topo_update_rest_test_sysdata_webclient_functions.app_config FROM web_anon;
	SET ROLE web_anon;
	SELECT 't2', jsonb_pretty(
		topo_update_rest.list_available_applications()
	);
ROLLBACK;

