#!/bin/sh

REGDIR=$(dirname $0)
TOPDIR=${REGDIR}/../..

export POSTGIS_REGRESS_DB=nibio_reg_rest
export PGTZ=utc

echo ARGS: $@

OPTS="--topology --extension"

if echo $@ | grep -q -- '--nocreate'; then
	:
else
	OPTS="${OPTS} --after-create-script ${REGDIR}/hooks/after_create_db_setup.sql"
fi

OPTS="${OPTS} --after-create-script ${REGDIR}/hooks/print_versions.sql"

if echo $@ | grep -q -- '--nodrop'; then
	:
else
	OPTS="${OPTS} --before-uninstall-script ${REGDIR}/hooks/before_db_drop_cleanup.sql"
fi



${REGDIR}/run_test.pl ${OPTS} $@

