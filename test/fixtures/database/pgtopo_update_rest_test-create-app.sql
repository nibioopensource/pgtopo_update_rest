SELECT * FROM topo_update.app_CreateSchema(
	'topo_update_rest_test',
	regexp_replace(
		$$
			{
				"version": "1.0",
				"topology": {
					"srid": 4326,
					"snap_tolerance": 1e-10
				},
				"tables": [
					{
						"name": "surface",
						"primary_key": "id",
						"topogeom_columns": [
							{
								"name": "geom",
								"type": "areal"
							}
						],
						"attributes": [
							{
								"name": "id",
								"type": "serial"
							},
							{
								"name": "label",
								"type": "text"
							}
						]
					},
					{
						"name": "border",
						"primary_key": "id",
						"topogeom_columns": [
							{
								"name": "geom",
								"type": "lineal"
							}
						],
						"attributes": [
							{
								"name": "id",
								"type": "serial"
							},
							{
								"name": "label",
								"type": "text"
							}
						]
					},
					{
						"name": "path",
						"primary_key": "id",
						"topogeom_columns": [
							{
								"name": "geom",
								"type": "lineal"
							}
						],
						"attributes": [
							{
								"name": "id",
								"type": "serial"
							},
							{
								"name": "label",
								"type": "text"
							}
						]
					},
					{
						"name": "poi",
						"primary_key": "id",
						"topogeom_columns": [
							{
								"name": "geom",
								"type": "puntal"
							}
						],
						"attributes": [
							{
								"name": "id",
								"type": "serial"
							},
							{
								"name": "label",
								"type": "text"
							}
						]
					},
					{
						"name": "data",
						"primary_key": "id",
						"attributes": [
							{
								"name": "id",
								"type": "serial"
							},
							{
								"name": "label",
								"type": "text"
							}
						]
					}
				],
				"surface_layer": {
					"table_name": "surface",
					"geo_column": "geom"
				},
				"border_layer": {
					"table_name": "border",
					"geo_column": "geom"
				},
				"path_layer": {
					"table_name": "path",
					"geo_column": "geom"
				},
				"point_layer": {
					"table_name": "poi",
					"geo_column": "geom"
				},
				"operations": {
					"GetFeaturesAsTopoJSON": {},
					"AddBordersSplitSurfaces": {},
					"AddPath": {},
					"SplitPaths": {},
					"RemovePath": {},
					"AddPoint": {},
					"MovePoint": {},
					"RemovePoint": {},
					"SurfaceMerge": {},
					"UpdateAttributes": {}
				}
			}
		$$,
		-- strip comments
		'\/\*.*?\*\/', '', 'g'
	)::jsonb
);

INSERT INTO topo_update_rest_test.data (label) VALUES ('initial');

