-- TODO: replace by topo_update.app_DropSchema when exposed
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/227

SELECT * FROM DropTopology('topo_update_rest_test_sysdata_webclient');

DROP SCHEMA IF EXISTS topo_update_rest_test CASCADE;

DROP SCHEMA IF EXISTS topo_update_rest_test_sysdata_webclient CASCADE;

DROP SCHEMA IF EXISTS topo_update_rest_test_sysdata_webclient_functions CASCADE;
