BEGIN;
SET constraints ALL IMMEDIATE;

TRUNCATE topo_update_rest_test.surface;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test.surface_id_seq',
	'1',
	false
);

TRUNCATE topo_update_rest_test.border;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test.border_id_seq',
	'1',
	false
);

TRUNCATE topo_update_rest_test.path;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test.path_id_seq',
	'1',
	false
);

TRUNCATE topo_update_rest_test.poi;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test.poi_id_seq',
	'1',
	false
);

TRUNCATE topo_update_rest_test.data;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test.data_id_seq',
	'1',
	false
);
INSERT INTO topo_update_rest_test.data (label) VALUES ('initial');

DELETE FROM topo_update_rest_test_sysdata_webclient.edge_data;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test_sysdata_webclient.edge_data_edge_id_seq',
	'1',
	false
);

DELETE FROM topo_update_rest_test_sysdata_webclient.node;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test_sysdata_webclient.node_node_id_seq',
	'1',
	false
);

DELETE FROM topo_update_rest_test_sysdata_webclient.face WHERE face_id > 0;
SELECT NULL FROM pg_catalog.setval(
	'topo_update_rest_test_sysdata_webclient.face_face_id_seq',
	'1',
	false
);

TRUNCATE topo_update_rest_test_sysdata_webclient.relation;

COMMIT;
