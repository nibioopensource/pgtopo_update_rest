DO
$$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = 'web_anon') THEN
	    CREATE ROLE web_anon nologin;
	END IF;
END;
$$;

GRANT usage ON SCHEMA topo_update_rest TO web_anon;

DO
$$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = 'authenticator_pgtopo_update_rest') THEN
	    CREATE ROLE authenticator_pgtopo_update_rest noinherit login PASSWORD 'mysecretpassword';
	END IF;
END;
$$;

GRANT web_anon TO authenticator_pgtopo_update_rest;
