CREATE OR REPLACE FUNCTION topo_update_rest.add_borders_split_surfaces(request JSONB)
RETURNS JSONB AS
$BODY$
DECLARE
	appname TEXT;
	borderset JSONB;
	ret JSONB;
BEGIN

	appname := request ->> 'app_name';
	IF appname IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'app_name', 'Missing request parameter'
			)
		);
	END IF;

	borderset := request -> 'borderset';
	IF borderset IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'borderset', 'Missing request parameter'
			)
		);
	END IF;

	SELECT
		jsonb_agg(to_jsonb(r))
	FROM topo_update.app_do_AddBordersSplitSurfaces(
		appname,
		borderset
	) r
	INTO ret;

	RETURN jsonb_build_object(
		'status', 'success',
		'data', ret
	);

EXCEPTION
WHEN raise_exception THEN
	RETURN jsonb_build_object(
		'status', 'fail',
		'data', jsonb_build_object(
			'raised_exception', jsonb_build_object(
				'message', SQLERRM
				-- TODO: add detail ?
			)
		)
	);
WHEN OTHERS THEN
	RETURN jsonb_build_object(
		'status', 'error',
		'message', format('Got exception %s (%s) -- am user %s', SQLERRM, SQLSTATE, current_user)
	);
END;
$BODY$
LANGUAGE 'plpgsql';

