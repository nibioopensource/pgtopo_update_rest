CREATE OR REPLACE FUNCTION topo_update_rest.update_attributes(request JSONB)
RETURNS JSONB AS
$BODY$
DECLARE
	appname TEXT;
	tabname TEXT;
	id TEXT;
	props JSONB;
	updated BOOLEAN;
BEGIN

	appname := request ->> 'app_name';
	IF appname IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'app_name', 'Missing request parameter'
			)
		);
	END IF;

	tabname := request ->> 'table_name';
	IF tabname IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'table_name', 'Missing request parameter'
			)
		);
	END IF;

	id := request ->> 'id';
	IF id IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'id', 'Missing request parameter'
			)
		);
	END IF;

	props := request -> 'properties';
	IF props IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'properties', 'Missing request parameter'
			)
		);
	END IF;

	SELECT topo_update.app_do_UpdateAttributes(
		appname,
		tabname,
		id,
		props
	)
	INTO updated;

	RETURN jsonb_build_object(
		'status', 'success',
		'data', updated
	);

EXCEPTION
WHEN raise_exception THEN
	--RAISE WARNING 'Got % (%)', SQLERRM, SQLSTATE;
	RETURN jsonb_build_object(
		'status', 'fail',
		'data', jsonb_build_object(
			'raised_exception', jsonb_build_object(
				'message', SQLERRM
				-- TODO: add detail ?
			)
		)
	);
WHEN OTHERS THEN
	--RAISE WARNING 'Got % (%)', SQLERRM, SQLSTATE;
	RETURN jsonb_build_object(
		'status', 'error',
		'message', format('Got exception %s (%s)', SQLERRM, SQLSTATE)
	);
END;
$BODY$
LANGUAGE 'plpgsql';


