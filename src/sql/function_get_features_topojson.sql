CREATE OR REPLACE FUNCTION topo_update_rest.get_features_topojson(request JSONB)
RETURNS JSONB AS
$BODY$
DECLARE
	appname TEXT;
	bbox_json JSONB;
	bbox_geom GEOMETRY;
	ret JSONB;
BEGIN

	appname := request ->> 'app_name';
	IF appname IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'app_name', 'Missing request parameter'
			)
		);
	END IF;

	bbox_json := request -> 'bbox_diagonal';
	IF bbox_json IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'bbox_diagonal', 'Missing request parameter'
			)
		);
	END IF;

	bbox_geom := ST_Envelope(ST_GeomFromGeoJSON( bbox_json ));

	SELECT topo_update.app_do_GetFeaturesAsTopoJSON(
		appname,
		bbox_geom
	)
	INTO ret;

	RETURN jsonb_build_object(
		'status', 'success',
		'data', ret
	);

EXCEPTION
WHEN raise_exception THEN
	--RAISE WARNING 'Got % (%)', SQLERRM, SQLSTATE;
	RETURN jsonb_build_object(
		'status', 'fail',
		'data', jsonb_build_object(
			'raised_exception', jsonb_build_object(
				'message', SQLERRM
				-- TODO: add detail ?
			)
		)
	);
WHEN OTHERS THEN
	--RAISE WARNING 'Got % (%)', SQLERRM, SQLSTATE;
	RETURN jsonb_build_object(
		'status', 'error',
		'message', format('Got exception %s (%s)', SQLERRM, SQLSTATE)
	);
END;
$BODY$
LANGUAGE 'plpgsql';


