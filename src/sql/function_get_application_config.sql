CREATE OR REPLACE FUNCTION topo_update_rest.get_application_config(request JSONB)
RETURNS JSONB AS
$BODY$
DECLARE
	appname TEXT;
	config_tab_schema TEXT;
	appconfig JSONB;
	sql TEXT;
BEGIN

	appname := request ->> 'app_name';
	IF appname IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'app_name', 'Missing request parameter'
			)
		);
	END IF;

	config_tab_schema = format('%s_sysdata_webclient_functions', appname);
	sql := format('SELECT cfg FROM %I.app_config', config_tab_schema);

	--RAISE WARNING 'SQL: %', sql;

	EXECUTE sql INTO appconfig;

	RETURN jsonb_build_object(
		'status', 'success',
		'data', appconfig
	);

EXCEPTION
WHEN undefined_table THEN
	RETURN jsonb_build_object(
		'status', 'fail',
		'message', format('Cannot find application %s', appname)
		--, 'detail', format('Got exception %s (%s)', SQLERRM, SQLSTATE)
	);
WHEN OTHERS THEN
	RETURN jsonb_build_object(
		'status', 'error',
		'message', format('Got exception %s (%s)', SQLERRM, SQLSTATE)
	);
END;
$BODY$
LANGUAGE 'plpgsql';

