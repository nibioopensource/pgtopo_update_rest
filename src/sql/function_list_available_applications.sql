CREATE OR REPLACE FUNCTION topo_update_rest.list_available_applications()
RETURNS JSONB AS
$BODY$
DECLARE
	ret JSONB;
BEGIN
	SELECT
		to_jsonb(
			array_agg(
				regexp_replace(
					t.name,
					'_sysdata_webclient$',
					''
				)
			)
		)
	FROM
		topology.topology t,
		pg_class c,
		pg_namespace n
	WHERE
		c.relnamespace = n.oid AND
		n.nspname = format('%s_functions', t.name) AND
		c.relname = 'app_config' AND
		pg_catalog.has_schema_privilege(current_user, n.oid, 'USAGE') AND
		pg_catalog.has_table_privilege(current_user, c.oid, 'SELECT')
	INTO ret;

	RETURN jsonb_build_object(
		'status', 'success',
		'data', ret
	);

EXCEPTION WHEN OTHERS THEN
	RETURN jsonb_build_object(
		'status', 'error',
		'message', format('Got exception %s (%s). I am %s', SQLERRM, SQLSTATE, current_user)
	);
END;
$BODY$
LANGUAGE 'plpgsql';
