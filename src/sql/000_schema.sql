CREATE SCHEMA IF NOT EXISTS topo_update_rest;

-- TODO find out where to have grant commands
GRANT USAGE ON SCHEMA topo_update_rest TO PUBLIC ;

-- Make a role to use for anonymous web requests.
-- When a request comes in, PostgREST will switch into this role in the database to run queries.
-- CREATE ROLE IF NOT EXISTS web_anon NOLOGIN;

-- GRANT USAGE ON SCHEMA topo_update_rest TO web_anon;
