VERSION = 1.0.0dev
PREFIX := /usr/local
SHAREDIR := $(PREFIX)/share
BINDIR := $(PREFIX)/bin
MODULEDIR := $(SHAREDIR)/topo_update-rest-$(VERSION)

INPUT_SQL := \
	src/sql/000_schema.sql \
	src/sql/function_add_borders_split_surfaces.sql \
	src/sql/function_add_path.sql \
	src/sql/function_add_point.sql \
	src/sql/function_get_application_config.sql \
	src/sql/function_get_features_topojson.sql \
	src/sql/function_list_available_applications.sql \
	src/sql/function_move_point.sql \
	src/sql/function_remove_borders_merge_surfaces.sql \
	src/sql/function_remove_path.sql \
	src/sql/function_remove_point.sql \
	src/sql/function_split_paths.sql \
	src/sql/function_update_attributes.sql \
	$(END)

BUILT_SCHEMA_LOADER_SCRIPTS := \
	topo_update_rest.sql \
	$(END)


all: $(BUILT_SCHEMA_LOADER_SCRIPTS) topo_update_rest.sh ## Build all targets

.PHONY: help
help: ## Print this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: check
check: $(BUILT_SCHEMA_LOADER_SCRIPTS) ## Run the testsuite
	$(MAKE) -C test/ check RUNTESTFLAGS=-v

topo_update_rest.sql: $(INPUT_SQL) Makefile
	echo "BEGIN;" > $@
	cat $(INPUT_SQL) >> $@
	echo "COMMIT;" >> $@

clean: ## Remove files created by make
	rm -f $(BUILT_SCHEMA_LOADER_SCRIPTS)
	rm -f topo_update_rest.sh
	$(MAKE) -C test/ $@

topo_update_rest.sh: topo_update_rest.sh.in
	cat $< | \
		sed 's|@@VERSION@@|$(VERSION)|' | \
		sed 's|@@MODULEDIR@@|$(MODULEDIR)|' > $@
	chmod +x $@

install: install-base install-bin ## Install all scripts

install-base: ## Install non-extension scripts
	mkdir -p $(MODULEDIR)
	cp -f $(BUILT_SCHEMA_LOADER_SCRIPTS) $(MODULEDIR)

install-bin: topo_update_rest.sh
	mkdir -p $(BINDIR)
	install $< $(BINDIR)
