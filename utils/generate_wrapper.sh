#!/bin/sh

WRAPPERNAME="$1"

test -n "${WRAPPERNAME}" || {
	echo "Usage: $0 <wrappername>" >&2
	exit 1
}

FUNCNAME="app_do_$(echo "${WRAPPERNAME}" | sed 's/_//g')"

ARGNAMETYPES=$(psql -XtAf - <<EOF
	SELECT
		replace(
			replace(
				pg_catalog.pg_get_function_identity_arguments(oid),
				' ',
				':'
			),
			',:',
			' '
		)
	FROM pg_proc
	WHERE proname = '${FUNCNAME}'
EOF
)

#echo "-- ARGNAMETYPES: ${ARGNAMETYPES}"

if test -z "${ARGNAMETYPES}"; then
	echo "ERROR: function $FUNCNAME does not exist or takes no arguments" >&2
	exit 1
fi


cat << EOF
CREATE OR REPLACE FUNCTION topo_update_rest.${WRAPPERNAME} (request JSONB)
RETURNS JSONB AS
\$BODY\$
DECLARE
	ret JSONB;
EOF

for argtype in $ARGNAMETYPES; do
	arg=$(echo $argtype | cut -d: -f1)
	typ=$(echo $argtype | cut -d: -f2-)
	cat << EOF
	$arg $typ;
EOF
done

cat << EOF
BEGIN
EOF

for argtype in $ARGNAMETYPES; do
	arg=$(echo $argtype | cut -d: -f1)
	typ=$(echo $argtype | cut -d: -f2-)
	argin="${arg}"
	if test "$argin" = "appname"; then
		argin="app_name" # for backward compatibility
	fi
	cat << EOF
	${arg} := request ->> '${argin}';
	IF ${arg} IS NULL THEN
		RETURN jsonb_build_object(
			'status', 'fail',
			'data', jsonb_build_object(
				'${argin}', 'Missing request parameter'
			)
		);
	END IF;
EOF
done

cat << EOF
	SELECT
		jsonb_agg(to_jsonb(r))
	FROM topo_update.${FUNCNAME}(
EOF

comma=
for argtype in $ARGNAMETYPES; do
	arg=$(echo $argtype | cut -d: -f1)
	printf "\t\t${comma}${arg}\n"
	comma=,
done

cat << EOF
	) r
	INTO ret;

	RETURN jsonb_build_object(
		'status', 'success',
		'data', ret
	);

EXCEPTION
WHEN raise_exception THEN
	RETURN jsonb_build_object(
		'status', 'fail',
		'data', jsonb_build_object(
			'raised_exception', jsonb_build_object(
				'message', SQLERRM
				-- TODO: add detail ?
			)
		)
	);
WHEN OTHERS THEN
	RETURN jsonb_build_object(
		'status', 'error',
		'message', format('Got exception %s (%s) -- am user %s', SQLERRM, SQLSTATE, current_user)
	);
END;
\$BODY\$
LANGUAGE 'plpgsql';
EOF
