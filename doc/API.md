# API

Every endpoint takes either no arguments or a single JSON object
and returns a JSON object with the top-level element specified
by the `jsend` specification:

	https://github.com/omniti-labs/jsend

Endpoint-specifics are documented in the corresponding endpoint
section

## Endpoint `list_available_applications`

Request takes no argument.

Response data will contain a list of strings representing
names of `topo_update` applications available to the connected user.
Example response:
```json
{
	"status": "success",
	"data": [ "ar5", "rein_nn", "rein_xx" ]
}
```

## Endpoint `get_application_config`

Request takes single `app_name` argument.
Example request:
```json
{ "app_name": "ar5" }
```

Response data will contain the full "application configuration"
as specified here:

	https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/develop/doc/APP_CONFIG.md

Example response:
```json
{
    "status": "success",
    "data": {
        "crs": {
            "type": "name",
            "properties": {
                "name": "EPSG:4326"
            }
        },
        "type": "Topology",
        "objects": {
            "arcs": {
                "arcs": [ [ [ 0, 0 ], [ 10, 0 ],
					[ 10, 10 ], [ 0, 10 ], [ 0, 0 ] ] ]
            },
            "collection": {
                "type": "GeometryCollection",
                "geometries": [
                    {
                        "arcs": [ [ 0 ] ],
                        "type": "MultiLineString",
                        "properties": {
                            "id": 1
                        }
                    },
                    {
                        "arcs": [ [ [ -1 ] ] ],
                        "type": "MultiPolygon",
                        "properties": {
                            "id": 1
                        }
                    }
                ]
            }
        }
    }
}
```

## Endpoint `get_features_topojson`

Request takes `app_name` and `bbox_diagonal` arguments,
where the `bbox_diagonal` element is a GeoJSON single-segment
linestring represeting the diagonal of a bounding box.

Example request:
```json
{
	"app_name": "ar5",
	"bbox_diagonal": {
		"type": "LineString",
		"crs": {
			"type": "name",
			"properties": {
				"name": "EPSG:4326"
			}
		},
		"coordinates": [[0,0],[10,10]]
	}
}
```

Response returns a TopoJSON objects containing all the
`border_layer` and `surface_layer` objects intersecting
the bounding box described by the given `bbox_diagonal`.


## Endpoint `add_borders_split_surfaces`

Request takes `app_name` and `borderset` arguments,
where the `borderset` element format specification is
specified here:

	https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/develop/src/sql/topo_update/function_03_app_do_AddBordersSplitSurfaces.sql#L361-427

Example request:
```json
{
	"app_name": "ar5",
	"borderset": {
		"geometry": { ... },
		"properties": {
			"Border": {
				"new": { "att1": "val1" },
				"modified": { "att2": "val2" },
				"split": { "att3": "val3" }
			},
			"Surface": {
				"new": { "att1": "val1" },
				"modified": { "att2": "val2" },
				"split": { "att3": "val3" }
			},
		}
	}
}
```

Response returns a list of objects describing all the
operations resulting from adding the border. Each element
of the list has members: `fid`, `typ`, `act`, `frm` as documented here:

	https://gitlab.com/nibioopensource/pgtopo_update_sql/-/blob/develop/src/sql/topo_update/function_02_add_border_split_surface.sql#L199-246

Example response:
```json
{
	"status": "success",
	"data": [
		{ "fid": "1", "typ": "B", "act": "M" },
		{ "fid": "2", "typ": "S", "act": "S", "frm": "1" },
		{ ... }
	}
}
```

### Endpoint `update_attributes`

Request takes `app_name`, `table_name`, `id` and `properties` arguments,
where the `table_name` is the name of a table found in the application
configuration as reported by `get_application_config` endpoint, `id`
is the text representation of the value of its primary key and `properties`
is an object with an element for each attribute that needs to be
updated.

Example request:
```json
{
	"app_name": "ar5",
	"table_name": "surface",
	"id": "4bbc34be-134e-11ed-bd1d-d1e67c53c75d"
	"properties": {
		"label": "updated"
	}
}
```

Response will just report status (success or failure).
In case of failure there will be a message detailing the failure.

Example response:
```json
{
    "status": "success"
}
```

